#include <iostream>
#include <string>
using namespace std;

//Разработчики
class Developer {
public:
	virtual void print() = 0;
	virtual ~Developer();
};

class CppDeveloper : public Developer {
public:
	void print() override {
		cout << "Im a cpp developer" << endl;
	}
};

class JavaDeveloper : public Developer {
public:
	void print() override {
		cout << "Im a java developer" << endl;
	}
};

class PhpDeveloper : public Developer {
public:
	void print() override {
		cout << "Im php developer" << endl;
	}
};

//Фабрики для создания разработчиков
class DeveloperFactory {
public:
	virtual Developer* createDeveloper() = 0;
	virtual ~DeveloperFactory();
};

class CppDeveloperFactory : public DeveloperFactory {
public:
	Developer * createDeveloper() {
		return new CppDeveloper();
	}
};

class JavaDeveloperFactory : public DeveloperFactory {
public:
	Developer * createDeveloper() {
		return new JavaDeveloper();
	}
};

class PhpDeveloperFactory : public DeveloperFactory {
public:
	Developer * createDeveloper() {
		return new  PhpDeveloper();
	}
};

DeveloperFactory* creteDeveloperBySpeciality(const string develop) {
	if (develop == "C++") {
		return new CppDeveloperFactory();
	}
	else if (develop == "Java") {
		return new JavaDeveloperFactory();
	}
	else if (develop == "Php") {
		return new PhpDeveloperFactory();
	} else 
	{
		return nullptr;
	}
}

int main() {
	setlocale(LC_ALL, "ru");
	DeveloperFactory* developerFactory = creteDeveloperBySpeciality("Php");
	Developer* developer =  developerFactory->createDeveloper();
	developer->print();
	system("pause");
	return 0;
}