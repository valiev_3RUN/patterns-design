#include <iostream>
using namespace std;
class Game {
public:
	void newGame() {
		cout << "new Game.." << endl;
	}
	void gameOver() {
		cout << "Game over.." << endl;
	}
};

class Command {
public:

	virtual void execute() = 0;
};

class NewGame : public Command {
private:
public:

	Game game;
	NewGame(Game& game) {
		this->game = game;
	}
	void execute() {
		game.newGame();
	}
};

class GameOver : public Command {
private:
	Game game;
public:

	GameOver(Game& game) {
		this->game = game;
	}
	void execute() {
		game.gameOver();
	}
};


class Player {
private:
	Command * _newGame, *_gameOver;
public:
	Player(Command* _newGame, Command* _gameOver) {
		this->_newGame = _newGame;
		this->_gameOver = _gameOver;
	}
	void NewGame() {
		_newGame->execute();
	}
	void GameOver() {
		_gameOver->execute();
	}
};

int main()
{
	Game game;
	Player player(new NewGame(game), new GameOver(game));
	player.NewGame();
	player.GameOver();
	system("pause");
	return 0;
}