#include <iostream>
using namespace std;
class Database {
public:
	void select() {
		cout << "selecting..." << endl;
	}
	void insert() {
		cout << "inserting.." << endl;
	}
	void update() {
		cout << "updating..." << endl;
	}

	void drop() {
		cout << "deleting.." << endl;
	}
};

class Command {
public:
	virtual void execute() = 0;
};

class Insert : public Command {
private:
	Database data;
public:
	Insert(Database& data) {
		this->data = data;
	}
	void execute() override {
		data.insert();
	}
};
class Select : public Command {
private:
	Database data;
public:
	Select(Database& data) {
		this->data = data;
	}
	void execute() override {
		data.select();
	}
};
class Update : public Command {
private:
	Database data;
public:
	Update(Database& data) {
		this->data = data;
	}
	void execute() override {
		data.update();
	}
};
class Drop : public Command {
private:
	Database data;
public:
	Drop(Database& data) {
		this->data = data;
	}
	void execute() override {
		data.drop();
	}
};


class Developer {
private:
	Command* select,*insert,*update, *drop;
public:

	Developer(Command* select, Command* insert, Command* update, Command* drop) {
		this->select = select;
		this->insert = insert;
		this->update = update;
		this->drop = drop;
	}
	
	void selectRecord() {
		select->execute();
	}
	void insertRecord() {
		insert->execute();
	}
	void updateRecord() {
		update->execute();
	}
	void dropRecord() {
		drop->execute();
	}
};

int main()
{
	Database database;
	Developer developer(new Select(database), new Insert(database), new Update(database), new Drop(database));
	developer.selectRecord();
	developer.updateRecord();
	developer.insertRecord();
	developer.dropRecord();
	system("pause");
	return 0;
}